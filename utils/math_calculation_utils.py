from ast import arg
from dataclasses import dataclass
from numpy.random import randint
from time import time_ns

def generate_number():
    return randint(1, 10)

    
def generate_numbers(n, dimension=1):
    if dimension > 1:
        return [generate_numbers(n, dimension-1) for _ in range(n)]
    else:
        return [generate_number() for _ in range(n)]

    
def current_nano_time():
    return time_ns()

def exec_time(func, *args):
    start_time = current_nano_time()

    result = func(*args)

    exec_time = current_nano_time() - start_time

    return Result(result, exec_time)

def are_matrixes_equal(mat1, mat2, *args):
    last_result = compute_hash(mat1.result)

    if last_result != compute_hash(mat2.result):
        return False

    return all(last_result == compute_hash(arg.result) for arg in args)

def compute_hash(matrix):
    return sum(map(sum, matrix)) if type(matrix) is list else sum(sum(matrix))

@dataclass(frozen=True)
class Result:
    result: any
    time: int 
